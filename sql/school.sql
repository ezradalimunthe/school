-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: school
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classinfo`
--

DROP TABLE IF EXISTS `classinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classinfo` (
  `id` varchar(10) NOT NULL,
  `aliasshort` varchar(255) NOT NULL COMMENT 'short name for this class',
  `aliaslong` varchar(255) DEFAULT NULL COMMENT 'long name for this class',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classinfo`
--

LOCK TABLES `classinfo` WRITE;
/*!40000 ALTER TABLE `classinfo` DISABLE KEYS */;
INSERT INTO `classinfo` VALUES ('10MM','10MM','10 Jurusan Multimedia'),('10RPL','10 RPL 2','10 Jurusan Rekayasa Perangkat Lunak 2'),('10RPL1','10 RPL 1','10 Jurusan Rekayasa Perangkat Lunak 1'),('10TKJ','10 TJK','10 Jurusan Teknik Jaringan Komputer'),('11MM','11MM','11 Jurusan Multimedia'),('11RPL','11 RPL','11 Jurusan Rekayasa Perangkat Lunak'),('11TKJ','11 TJK','11 Jurusan Teknik Jaringan Komputer'),('12RPL','12 RPL','12 Jurusan Rekayasa Perangkat Lunak'),('12TKJ','12 TJK','12 Jurusan Teknik Jaringan Komputer');
/*!40000 ALTER TABLE `classinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL COMMENT 'student firstname',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'student lastname',
  `gender` varchar(255) DEFAULT NULL COMMENT 'student firstname',
  `address` varchar(255) DEFAULT NULL COMMENT 'student home address',
  `city` varchar(255) DEFAULT NULL COMMENT 'student home city',
  `Phone` varchar(100) DEFAULT NULL COMMENT 'parent phone',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1000,'Lila','Day','F','8103 In St.','Binjai','186-8285'),(1001,'Tamekah','Morse','F','P.O. Box 360, 3074 Donec St.','Medan','918-9797'),(1002,'Burton','Santos','M','Ap #483-4195 Dolor. Avenue','Binjai','672-2005'),(1003,'Maryam','Nolan','M','P.O. Box 224, 1737 Amet, Street','Medan','+628876-0282-83814'),(1004,'Tucker','Hunt','M','P.O. Box 876, 9636 Ullamcorper, Rd.','Binjai','199-6340'),(1005,'Wilma','Cunningham','M','Ap #569-1348 Posuere, Road','Medan','615-7225'),(1006,'Ruth','Garner','F','9692 Nascetur Rd.','Medan','+628153-2332-12239'),(1007,'Nathan','West','F','346-5725 Placerat, St.','Medan','+628808-4178-15332'),(1008,'Ora','Flores','F','P.O. Box 420, 9983 Et St.','Medan','269-4682'),(1009,'Ross','Blankenship','M','9175 Integer Avenue','Binjai','+628617-5363-87101'),(1010,'Marcia','Carey','M','Ap #891-5677 Nulla Ave','Binjai','269-4445'),(1011,'Austin','Trujillo','F','P.O. Box 174, 8934 Id Ave','Binjai','+628125-3504-36587'),(1012,'Mona','Boyer','F','948-5277 Euismod St.','Medan','+628644-3904-80222'),(1013,'Kessie','Nichols','F','866-2200 Curabitur St.','Binjai','+628137-1438-41848'),(1014,'Hunter','Mueller','M','P.O. Box 328, 1737 Faucibus St.','Medan','159-1960'),(1015,'Connor','Tyson','F','1268 Magna, Avenue','Binjai','+628266-9939-59009'),(1016,'Harding','Abbott','F','P.O. Box 260, 8294 Enim, St.','Medan','+628084-9398-15581'),(1017,'Cullen','Walls','M','Ap #575-8461 Quis Street','Medan','611-2375'),(1018,'Dustin','Meyer','F','7191 Libero Av.','Binjai','+628174-7834-02780'),(1019,'Myra','Chase','M','819-618 Vitae St.','Medan','+628629-2204-88763'),(1020,'Jamal','Duffy','F','Ap #130-5861 Sit Road','Binjai','+628355-1577-65079'),(1021,'Bryar','Powers','M','Ap #830-7906 Dui Road','Medan','+628356-0029-79496'),(1022,'Yoko','Leonard','M','7031 A, Ave','Binjai','377-4160'),(1023,'Ivana','Blackburn','M','Ap #379-4286 Nonummy Road','Medan','+628784-1066-77028'),(1024,'Emerald','Heath','M','Ap #931-9368 Fermentum Ave','Binjai','788-0067'),(1025,'Sybil','Horton','M','695-2966 Eu Ave','Binjai','+628414-3392-84967'),(1026,'Jescie','Rivers','M','Ap #225-9984 Eget Ave','Binjai','226-1408'),(1027,'Hedy','Madden','M','9699 Dapibus Av.','Medan','+628407-3753-68988'),(1028,'Kevin','Fitzgerald','F','738-3313 Senectus Street','Medan','+628727-2478-49294'),(1029,'Jamalia','Byers','F','P.O. Box 563, 3717 Lorem Rd.','Binjai','677-8135'),(1030,'Colleen','Lawson','M','P.O. Box 821, 8861 Risus. Ave','Binjai','+628264-5554-74362'),(1031,'Lacy','Ingram','M','Ap #649-4708 Enim, Street','Medan','+628989-7038-26288'),(1032,'Alika','Williams','F','Ap #107-2673 Suspendisse Rd.','Binjai','+628643-0130-29520'),(1033,'Carol','Carroll','M','P.O. Box 649, 8188 Lobortis. Av.','Binjai','+628464-1036-11637'),(1034,'Amy','Alvarez','F','P.O. Box 793, 2326 Augue St.','Binjai','+628870-0085-89853'),(1035,'Macaulay','Gould','F','453-7951 Orci Ave','Medan','498-2989'),(1036,'Deborah','Cervantes','F','Ap #847-9779 Malesuada Rd.','Medan','+628080-8929-95200'),(1037,'Lana','Melendez','M','Ap #335-3798 Penatibus Street','Binjai','+628168-3329-51632'),(1038,'Martin','Welch','M','Ap #627-996 Suspendisse Ave','Binjai','726-6080'),(1039,'Candice','Rice','M','182-3307 Donec Rd.','Medan','821-8594'),(1040,'Elijah','Santiago','M','4322 Erat Street','Medan','+628375-3911-21033'),(1041,'Zahir','Russo','M','Ap #756-8986 Maecenas St.','Binjai','419-9943'),(1042,'Isaiah','Gomez','F','4087 Non Rd.','Medan','917-9317'),(1043,'Neil','Anthony','M','Ap #768-1272 Turpis. Avenue','Binjai','+628018-1488-74367'),(1044,'Guinevere','Rogers','F','P.O. Box 643, 4145 Massa. St.','Binjai','+628683-5849-39975'),(1045,'Sandra','Neal','F','P.O. Box 183, 9488 Elit St.','Binjai','+628123-2074-43792'),(1046,'Lacey','Schneider','M','P.O. Box 255, 9084 Et St.','Medan','752-9041'),(1047,'Liberty','Barber','M','P.O. Box 925, 8610 Mollis. St.','Medan','353-3958'),(1048,'Louis','Luna','F','526-6913 Nec St.','Binjai','724-7017'),(1049,'Malcolm','Beck','M','869-3176 Ullamcorper Road','Binjai','227-1640'),(1050,'Wendy','Faulkner','F','Ap #204-4240 Vitae, Street','Binjai','+628267-1839-18394'),(1051,'Harlan','Mccullough','F','1872 Facilisis Av.','Medan','564-5801'),(1052,'Colleen','Hays','F','514-9130 Elementum Ave','Medan','873-0160'),(1053,'Armando','Bryant','M','3812 Amet, Street','Medan','858-4472'),(1054,'Elmo','Rush','M','P.O. Box 362, 4721 Urna. St.','Binjai','+628702-6040-65842'),(1055,'Clare','Duran','M','P.O. Box 624, 7509 Turpis Road','Binjai','480-8225'),(1056,'Nolan','Goff','F','5868 Sagittis Rd.','Medan','646-2034'),(1057,'Jaquelyn','Norman','F','2591 Lectus St.','Binjai','+628872-7107-09257'),(1058,'Shoshana','Bird','F','498-3372 Dolor. Street','Binjai','+628074-3338-52073'),(1059,'Wyoming','Cardenas','M','770-3026 Gravida Av.','Medan','502-1553'),(1060,'Delilah','Albert','M','P.O. Box 888, 3092 Nec Ave','Binjai','988-7452'),(1061,'Emmanuel','Ball','M','2894 Suspendisse Av.','Binjai','133-2765'),(1062,'Sophia','Singleton','M','251 Dolor Road','Medan','+628439-7727-10272'),(1063,'Dominic','Hull','M','1461 Velit Rd.','Medan','753-6271'),(1064,'Barbara','Villarreal','M','5347 Varius Road','Medan','+628145-3760-19201'),(1065,'Alec','Santana','F','P.O. Box 182, 9661 Vivamus Rd.','Binjai','+628927-8272-73115'),(1066,'Mira','Sharpe','F','475-7519 Auctor Av.','Medan','+628353-0715-92022'),(1067,'Chandler','Pratt','M','P.O. Box 108, 4271 Odio. Road','Binjai','+628872-4436-20361'),(1068,'Rhona','Hardy','F','6581 Auctor Ave','Binjai','+628746-7485-74972'),(1069,'Dai','Galloway','F','Ap #670-8885 Cras Street','Binjai','659-7541'),(1070,'Tad','Mays','M','Ap #638-9727 Lacus, St.','Medan','+628691-7819-74159'),(1071,'Madonna','Branch','F','7931 Dictum Rd.','Binjai','+628586-7462-83477'),(1072,'Dominic','Knox','F','P.O. Box 103, 7502 Neque St.','Medan','314-4919'),(1073,'Arden','Reed','F','498-2198 Vitae Avenue','Binjai','+628655-9145-50124'),(1074,'Alice','Moore','F','P.O. Box 658, 5842 Vitae St.','Binjai','+628149-6017-67931'),(1075,'Maya','Colon','F','9588 Sem. Road','Binjai','553-5706'),(1076,'Peter','Cummings','F','8821 Lobortis Road','Medan','369-1807'),(1077,'Nelle','Dennis','F','P.O. Box 563, 7327 Quisque Ave','Binjai','576-4616'),(1078,'Sheila','Davenport','F','6324 Rutrum St.','Binjai','+628830-3225-07960'),(1079,'Quincy','Snyder','F','Ap #417-3281 Ac St.','Binjai','+628116-0894-06827'),(1080,'Luke','Chapman','F','692-1218 Magnis Rd.','Binjai','277-7342'),(1081,'Lucas','Vance','F','Ap #404-2147 Metus Avenue','Binjai','136-4191'),(1082,'Breanna','Washington','F','P.O. Box 316, 8652 Semper. Street','Binjai','+628409-0869-48614'),(1083,'Ariana','Hendricks','M','P.O. Box 566, 628 Scelerisque Rd.','Binjai','345-7469'),(1084,'Kiona','Cohen','F','6998 Natoque St.','Medan','+628036-9640-79931'),(1085,'Rhonda','Goodman','M','4239 Tellus St.','Binjai','469-1171'),(1086,'Xavier','Santana','F','Ap #410-1880 Urna. St.','Medan','328-4671'),(1087,'Hayfa','Maddox','F','P.O. Box 848, 2480 Ipsum Rd.','Medan','760-3004'),(1088,'Rigel','Barber','M','592-4177 Non Ave','Medan','+628860-3597-60961'),(1089,'Maryam','Mendez','F','Ap #616-7644 Vulputate, St.','Binjai','672-4939'),(1090,'Aileen','Lee','M','Ap #859-360 A St.','Binjai','+628081-1257-29420'),(1091,'Tanya','Brennan','M','Ap #156-7624 Placerat St.','Medan','+628843-5939-78196'),(1092,'Faith','Roach','F','541-8012 Risus Street','Binjai','409-8677'),(1093,'Ebony','Watson','M','P.O. Box 455, 2094 Varius Av.','Binjai','386-5390'),(1094,'Louis','Kemp','F','767 Integer Ave','Medan','593-5589'),(1095,'Kessie','Moore','F','818-1146 Egestas Av.','Medan','851-9813'),(1096,'Kiara','Holloway','F','2853 Feugiat Av.','Medan','+628228-6153-53038'),(1097,'Lacy','Elliott','F','Ap #330-1882 Gravida Ave','Binjai','783-2977'),(1098,'Aurora','Wade','M','Ap #452-7356 Donec St.','Medan','+628736-4168-59987'),(1099,'Murphy','Hooper','M','Ap #698-3016 Neque St.','Binjai','113-4645');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_class`
--

DROP TABLE IF EXISTS `student_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_class` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `studentid` mediumint(9) DEFAULT NULL COMMENT 'fk from table student id',
  `classid` varchar(255) DEFAULT NULL COMMENT 'class id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_class`
--

LOCK TABLES `student_class` WRITE;
/*!40000 ALTER TABLE `student_class` DISABLE KEYS */;
INSERT INTO `student_class` VALUES (1,1046,'10RPL'),(2,1017,'11RPL'),(3,1010,'10RPL'),(4,1086,'11MM'),(5,1093,'11TKJ'),(6,1046,'11MM'),(7,1054,'12RPL'),(8,1017,'10RPL'),(9,1045,'11MM'),(10,1083,'11MM'),(11,1043,'10TKJ'),(12,1014,'11MM'),(13,1050,'12RPL'),(14,1073,'11RPL'),(15,1039,'10MM'),(16,1096,'11TKJ'),(17,1028,'10MM'),(18,1076,'11MM'),(19,1078,'12RPL'),(20,1042,'12RPL'),(21,1061,'12TKJ'),(22,1099,'10MM'),(23,1091,'11RPL'),(24,1046,'11TKJ'),(25,1046,'12MM'),(26,1000,'10RPL'),(27,1012,'12RPL'),(28,1022,'10TKJ'),(29,1041,'12RPL'),(30,1054,'12TKJ'),(31,1038,'11TKJ'),(32,1044,'12MM'),(33,1050,'10TKJ'),(34,1028,'10TKJ'),(35,1069,'12RPL'),(36,1033,'11MM'),(37,1038,'12MM'),(38,1042,'12RPL'),(39,1017,'11MM'),(40,1044,'10MM'),(41,1059,'12TKJ'),(42,1092,'12MM'),(43,1002,'11TKJ'),(44,1096,'12TKJ'),(45,1029,'11TKJ'),(46,1036,'12RPL'),(47,1022,'11TKJ'),(48,1047,'12TKJ'),(49,1040,'10RPL'),(50,1032,'10MM'),(51,1029,'11RPL'),(52,1017,'11TKJ'),(53,1003,'10MM'),(54,1095,'11MM'),(55,1010,'10RPL'),(56,1082,'12TKJ'),(57,1050,'10RPL'),(58,1084,'10MM'),(59,1075,'12MM'),(60,1089,'11TKJ'),(61,1097,'12RPL'),(62,1052,'12TKJ'),(63,1066,'10MM'),(64,1077,'12MM'),(65,1012,'10RPL'),(66,1097,'12TKJ'),(67,1083,'10MM'),(68,1004,'10RPL'),(69,1033,'11MM'),(70,1066,'12RPL'),(71,1040,'12MM'),(72,1006,'11MM'),(73,1014,'10RPL'),(74,1090,'11RPL'),(75,1019,'10RPL'),(76,1064,'11TKJ'),(77,1097,'11MM'),(78,1011,'10TKJ'),(79,1034,'10RPL'),(80,1075,'10MM'),(81,1068,'10MM'),(82,1060,'11MM'),(83,1054,'11TKJ'),(84,1073,'11TKJ'),(85,1040,'11TKJ'),(86,1022,'11TKJ'),(87,1002,'12TKJ'),(88,1065,'11TKJ'),(89,1051,'10MM'),(90,1073,'12RPL'),(91,1028,'12RPL'),(92,1064,'12MM'),(93,1052,'12TKJ'),(94,1026,'12MM'),(95,1010,'10TKJ'),(96,1020,'11RPL'),(97,1065,'11RPL'),(98,1056,'11TKJ'),(99,1068,'12TKJ'),(100,1096,'12MM');
/*!40000 ALTER TABLE `student_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'subject name',
  `abbv` varchar(255) DEFAULT NULL COMMENT 'subject abbreviation',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'Pendidikan Agama dan Budi Pekerti','AGAMA'),(2,'Pendidikan Pancasila dan Kewarganegaraan','PPKN'),(3,'Bahasa Indonesia','BINDO'),(4,'Matematika','MTK'),(5,'Sejarah Indonesia','Sejarah'),(6,'Bahasa Inggris','ENG');
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher` (
  `id` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `gender` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES ('10-7444268','Ancell','Mostin','amostin0@loc.gov','Male'),('64-1928740','Earle','Larrat','elarrat1@seesaa.net','Male'),('81-5874863','Catlee','Lindblad','clindblad2@biblegateway.com','Female'),('06-1812193','Marie','Tibbetts','mtibbetts3@illinois.edu','Female'),('96-0687586','Nikolaos','Keable','nkeable4@discuz.net','Male'),('49-1770641','Christal','Botwood','cbotwood5@devhub.com','Female'),('17-6925617','Quintana','Ebbetts','qebbetts6@nydailynews.com','Female'),('92-7032588','Steffi','Jakubski','sjakubski7@godaddy.com','Female'),('17-0631101','Ina','Langthorne','ilangthorne8@sitemeter.com','Female'),('50-4008976','Lara','Daggett','ldaggett9@wikispaces.com','Female'),('31-6341038','Alaine','Loweth','alowetha@bigcartel.com','Female');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_subject`
--

DROP TABLE IF EXISTS `teacher_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_subject` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `teacherid` varchar(50) DEFAULT NULL COMMENT 'fk from table teacher id',
  `subjectid` mediumint(9) DEFAULT NULL COMMENT 'fk from table subject id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_subject`
--

LOCK TABLES `teacher_subject` WRITE;
/*!40000 ALTER TABLE `teacher_subject` DISABLE KEYS */;
INSERT INTO `teacher_subject` VALUES (1,'10-7444268',2),(2,'64-1928740',1),(3,'81-5874863',3),(4,'06-1812193',5),(5,'96-0687586',4),(6,'49-1770641',4),(7,'64-1928740',2);
/*!40000 ALTER TABLE `teacher_subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-30 14:04:31
