<?php
use app\assets\DatatableNetAsset;
use app\components\Breadcrumb;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

DatatableNetAsset::register($this);

$this->registerJsFile(
    '@web/js/classinfo/index.js',
    ['depends' => [\app\assets\DatatableNetAsset::className()]]
);

$classinfoArray = ArrayHelper::map($classinfo, 'id', 'aliasshort');

?>
<?=Breadcrumb::widget(['title' => 'Daftar Kelas', 'icon' => 'fa fa-bar-chart'])?>
<div class="row">
  <div class="col-sm-12">
    <div class="tile">
      <div class="tile-title">Daftar Siswa Per Kelas</div>
      <div class="tile-body">
          <!--form-->
          <?=Html::beginForm(['classinfo/getstudentlist'], 'post', ["class" => "row"]);?>
                <div class="form-group col-md-3">
                    <label class="control-label">Kelas</label>
                  </div>
                  <div class="form-group col-md-3">
                    <?=Html::dropDownList("classname", null, $classinfoArray,
                         ["class" => "form-control","id"=>"classname"]);?>
                  </div>
                  <div class="form-group col-md-4 align-self-end">
                    <button class="btn btn-primary" id="btnViewClass" type="button">
                      <i class="fa fa-fw fa-lg fa-search"></i>Lihat</button>
                  </div>
          <?=Html::endForm()?>
          <form class="row">

                </form>
          <!--end form-->

      </div>
  </div>
  </div>
</div>
<div class="tile">
  <div class="tile-body">
  <div class="table-responsive">
  <table class="table table-bordered table-striped table-hover" id="studenttable">
    <caption>Daftar Siswa</caption>
      <thead>
        <tr>
          <th class="col-sm-1">NIS</th>
          <th class="col-sm-3">Nama Lengkap</th>
          <th class="col-sm-4">Alamat</th>
          <th class="col-sm-1">Gender</th>
          <th class="col-sm-1">Phone</th>

        </tr>
      </thead>
    </table>
  </div>
  
  </div>
</div>