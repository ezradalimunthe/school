<?php

use yii\helpers\Html;

?>
 <!-- Sidebar menu-->
 <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">Nama Guru.</p>
          <p class="app-sidebar__user-designation">NIK</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="/"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li class="treeview">
            <a class="app-menu__item active" href="#" data-toggle="treeview">
                <i class="app-menu__icon fa fa-file-text"></i>
                <span class="app-menu__label">Entitas</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
          <ul class="treeview-menu">
            <li><a class="treeview-item active" href="/classinfo"><i class="icon fa fa-circle"></i> Kelas</a></li>
            <li><a class="treeview-item" href="teacher"><i class="icon fa fa-circle-o"></i> Guru </a></li>
            <li><a class="treeview-item" href="student"><i class="icon fa fa-circle-o"></i> Siswa </a></li>
          </ul>
        </li>
        <li><a class="app-menu__item" href="/absensi"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Absensi</span></a></li>
        <li class="treeview">
            <a class="app-menu__item " href="#" data-toggle="treeview">
                <i class="app-menu__icon fa fa-file-text"></i>
                <span class="app-menu__label">Analyzer</span>
                <i class="treeview-indicator fa fa-angle-right"></i>
            </a>
          <ul class="treeview-menu">
            <li><?=Html::a("<i class='icon fa fa-circle-o'></i> Page Analyzer",["/analyzer/page-analyzer"],["class"=>"treeview-item"]);?></li>
            <li><?=Html::a("<i class='icon fa fa-circle-o'></i> Database Analyzer",["/analyzer/database-analyzer"],["class"=>"treeview-item"]);?></li>
            
          </ul>
        </li>
      </ul>
    </aside>
   