<?php
use app\assets\DatatableNetAsset;
use app\components\Breadcrumb;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

DatatableNetAsset::register($this);

$this->registerJsFile(
    '@web/js/teacher/index.js',
    ['depends' => [\app\assets\DatatableNetAsset::className()]]
);

?>
<div class="tile">
  <div class="tile-body">
  <div class="table-responsive">
  <table class="table table-bordered table-striped table-hover" id="teacher">
    <caption>Daftar Guru</caption>
      <thead>
        <tr>
          <th class="col-sm-1">ID</th>
          <th class="col-sm-2">First Name</th>
          <th class="col-sm-2">Last Name</th>
          <th class="col-sm-2">Email</th>
          <th class="col-sm-2">Gender</th>

        </tr>
      </thead>
    </table>
  </div>
  
  </div>
</div>