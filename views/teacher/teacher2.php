<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = "Data guru";
?>

<div class="row">
    <div class="col-md-12">
    <div class="tile">
        <div class="tile-body">
            <div class='row'>
                <div class="col-md-2">
                    <div class="text-center">
                        <img class="rounded mx-auto d-block"
                        src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg" />
                    </div>

                </div>
                <div class="col-md-10">
                    <div class="tile">
                        <div class="tile-title-w-btn">
                            <h3 class="title">Biodata</h3>
                            <p><?=Html::a("<i class='fa fa-edit'></i>Edit", 
                                            ["student/update", "id" => $model->id], 
                                            ["class" => "btn btn-primary icon-btn"]);?>
                            </p>
                        </div>
                        <div class="tile-body">
<!--a-->
                            <div class="row">
                                <div class="col-md-2"><label>ID</label></div>
                                <div class="col-md-8"><p><?=$model->id?> </p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Nama Depan</label></div>
                                <div class="col-md-8"><p><?=$model->firstname?> </p></div>
                            </div>
                            <div>
                                <div class="col-md-2"><label>Nama Belakang</label></div>
                                <div class="col-md-8"><p><?=$model->lastname;?> </p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Email</label></div>
                                <div class="col-md-8"><p><?=$model->email?></p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Gender</label></div>
                                <div class="col-md-8"><p><?=$model->gender?></p></div>
                            </div>

<!--aend-->
                        </div>
                    </div>

                </div>
            </div>



        </div>

    </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="tile">
            <div class="tile-header">
                <h3>Prestasi</h3>
            </div>
            <div class="tile-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="tile">
            <div class="tile-header">
                <h3>Peringatan / Hukuman</h3>
            </div>
            <div class="tile-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


