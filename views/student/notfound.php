<?php
use app\components\Breadcrumb;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = "Data siswa";
?>
<?=Breadcrumb::widget(['title'=>'Data Siswa', 'icon'=>'fa fa-address-card-o'])?>


<div class="row">
    <div class="col-md-12">
        <div class="tile text-white bg-warning">
            <div class="tile-header text-center">
                <h3>Data tidak ditemukan !</h3>
            </div>
            <div  class="tile-body">
                <p> Data siswa dengan Nomor Induk Siswa <strong><?=$id;?> tidak ada.</p>
            </div>
        </div>
        
    </div>
</div>
