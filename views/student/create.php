<?php
use app\components\Breadcrumb;

$this->registerJsFile(
    '@web/js/student/form.js',
    ['depends' => [\app\assets\AppAsset::className()]]
  );
  $this->title = "Data Siswa"
?>

<?=Breadcrumb::widget(['title'=>$this->title, 'icon'=>'fa fa-address-card-o'])?>
<?= $this->render('_form', [
    'model' => $model,
    "action"=>"create"
]) ?>