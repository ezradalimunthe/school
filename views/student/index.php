<?php
use yii\helpers\Html;
use app\components\Breadcrumb;
/* @var $this yii\web\View */
$this->title = "Data siswa";
?>
<?=Breadcrumb::widget(['title'=>$this->title, 'icon'=>'fa fa-address-card-o'])?>
<div class="row">
    <div class="col-md-12">
    <div class="tile">
        <div class="tile-body">
            <div class='row'>
                <div class="col-md-2">
                    <div class="text-center">
                    <?=Html::img(["/multimedia/profile-picture","id"=>$model->id],["width"=>"100px",
                    "class"=>"rounded mx-auto d-block"])?>
                    
                    </div>

                </div>
                <div class="col-md-10">
                    <div class="tile">
                        <div class="tile-title-w-btn">
                            <h3 class="title">Biodata</h3>
                            <p><?=Html::a("<i class='fa fa-edit'></i>Edit", 
                                            ["student/update", "id" => $model->id], 
                                            ["class" => "btn btn-primary icon-btn"]);?>
                            </p>
                        </div>
                        <div class="tile-body">
<!--a-->
                            <div class="row">
                                <div class="col-md-2"><label>NIS</label></div>
                                <div class="col-md-8"><p><?=$model->id?> </p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Nama</label></div>
                                <div class="col-md-8"><p><?=$model->firstname?> <?=$model->lastname;?></p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Alamat</label></div>
                                <div class="col-md-8"><p><?=$model->address?></p></div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label>Kota</label></div>
                                <div class="col-md-8"><p><?=$model->city?></p></div>
                            </div>

<!--aend-->
                        </div>
                    </div>

                </div>
            </div>



        </div>

    </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="tile">
            <div class="tile-header">
                <h3>Prestasi</h3>
            </div>
            <div class="tile-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="tile">
            <div class="tile-header">
                <h3>Peringatan / Hukuman</h3>
            </div>
            <div class="tile-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


