<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Button;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form ActiveForm */

function inputSize($size = 'medium')
{
    $css = 'col-sm-2 col-md-4';
    switch ($size) {
        case "short":
            $css = "col-sm-2 col-md-2";
            break;
        case "long":
            $css = "col-sm-4 col-md-4";
        default:

    }
    return [
        'horizontalCssClasses' => [
            'wrapper' => $css,
            'label' => 'col-sm-2 col-md-2',
        ],
    ];
};
$nowrapper = [
    'horizontalCssClasses' => [
        'field' => 'col-sm-5',
    ],
    'template' => '{input}{error}{hint}',
    'options'=>["style"=>"padding-left:10px!important;"]

];

$studentClass = is_null( $model->class)?"[belum ada kelas]":$model->class->aliasshort;
$urlAction = Url::toRoute(["student/update","id"=>$model->id]);
if ($action =="create"){
    $urlAction = Url::toRoute(["student/create"]);
}
?>

        <div class="tile">
            <div class="tile-body">
                <div class="row">
                    <div class="col-sm-6 col-md-3 user">
                    <div class="profile">
                        <div class="info">
                        <?=Html::img(["/multimedia/profile-picture","id"=>$model->id],["width"=>"100px",
                        "class"=>"user-img"])?>
                            <h4><?=$model->firstname?> <?=$model->lastname?> </h4>
                            <p><?=$studentClass;?></p>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-6 col-md-9">
                        <?php $form = ActiveForm::begin(['layout' => 'horizontal',
                        'validateOnSubmit'=>true,
                        'validateOnBlur'=>false,
                        'validateOnType' =>false,
                        'validateOnChange'=>false,
                        'enableClientValidation'=>false,
                        'enableAjaxValidation'=>true,
                        'validationUrl'=>$urlAction,
                        'options'=>['autocomplete'=>"off", 'id'=>'form-data-siswa'] ]);?>

                        <?=$form->field($model, 'id',inputSize("short"))?>
                      
                        <div class="form-row mb-3">
                        <label class="col-sm-2 col-form-label" for="student-firstname">Nama Lengkap</label>
                            <?=$form->field($model, 'firstname', $nowrapper)?>
                            <?=$form->field($model, 'lastname', $nowrapper)?>
                                    
                        </div>

                            <?=$form->field($model, 'gender')->inline()->radioList(["M" => "Male", "F" => "Female"])?>
                            <?=$form->field($model, 'address')?>
                            <?=$form->field($model, 'city', inputSize("medium"))?>
                            <?=$form->field($model, 'Phone', inputSize("medium"))?>
                            <div class="form-group">
                            <?=Html::resetButton('Reset', ['class' => 'btn btn-warning']) ?>
                            <?=Html::submitButton("Simpan", ['class' => 'btn btn-success','id'=>'btnSubmit']) ?>
                            </div>
                        <?php ActiveForm::end();?>
                    </div>
                </div>
            </div>
        </div>


