<?php

namespace app\controllers;

use app\models\Student;
use Yii;
use yii\base\DynamicModel;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

class StudentController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        $model = Student::findOne($id);
        if (is_null($model)) {
            return $this->render("notfound", ['id' => $id]);
        }
        return $this->render('index', ["model" => $model]);
    }

    public function actionCreate(){
        $model = new Student;
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            return $this->saveStudentData($model);
        }
        return $this->render('create', ["model" => $model]);
    }
    public function actionUpdate($id)
    {
        $model = Student::findOne($id);

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            //$model=NULL;
            if (is_null($model)) {
                $model = new Student;
                $properties = [];
                foreach ($model as $key => $value) {
                    $properties[] = "student-" . strtolower($key);
                }
                $properties[] = "error";
                $returnvalue = new DynamicModel($properties);
                foreach ($returnvalue as $key => $value) {
                    $returnvalue[$key] = [""];
                }
                $returnvalue->error = "Data tidak dapat diupdate. Kemungkinan data telah dihapus!<br/>" . 
                    Html::a("Refresh halaman ini", ["student/update","id"=>$id]) .
                    " untuk melihat ketersediaan data.";
                return $this->asJson($returnvalue);
            };
            $model->load(Yii::$app->request->post());
          return $this->saveStudentData($model);
        }

        if (is_null($model)) {
            return $this->render("notfound", ['id' => $id]);
        }
        return $this->render("update", ["model" => $model]);

    }

    private function saveStudentData($model)
    {
        $returnvalue = [];
     
        if ($model->validate()) {
            $model->save();

        } else {
            $returnvalue = ActiveForm::validate($model);
        }

        return $this->asJson($returnvalue);
    }
}
