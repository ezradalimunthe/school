<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Query;


class TeacherController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
public function actionGetTeacherList(){
        $request = Yii::$app->request;
      
        $teacher = (new Query)->select('id, first_name, last_name, email, gender')
        ->from ("teacher")
        
        
        ->all();
        

        return $this->asJson($teacher);

    }
}
