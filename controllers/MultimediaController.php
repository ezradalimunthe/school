<?php
namespace app\controllers;
use Yii;
use app\models\Student;
use yii\helpers\Html;
use yii\helpers\FileHelper;

use yii\imagine\Image;

class MultimediaController extends \yii\web\Controller
{
    /**
     * render profile picture
     */
    public function  actionProfilePicture($id)
    {
        //create the path;
        $picturefile  = FileHelper::normalizePath( Yii::getAlias("@multimedia").
                         "/profile-picture/" . $id .".jpg");
        if (!file_exists($picturefile)){
            $picturefile  = FileHelper::normalizePath(  Yii::getAlias("@multimedia").
            "/profile-picture/default-profile.jpg");
        }
       
        $image = Image::getImagine();
        $image->open ($picturefile)
            ->show("jpg");
                     
        exit;


    }
}