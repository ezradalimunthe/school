<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Classinfo;
use app\models\StudentClass;
use yii\db\Query;




class ClassinfoController extends Controller
{
    public function actionIndex()
    {
        $classinfo= Classinfo::find()->all();
        return $this->render("index",[
            "classinfo"=>$classinfo
        ]);
    }

    /**
     * get student list from class
     * return json
     */
    public function actionGetstudentlist(){
        $request = Yii::$app->request;
        $classname = $request->post("classname");
        $students = (new Query)->select('student.id, firstname, lastname, gender, address, city,phone')
        ->from ("student")
        ->innerJoin("student_class", "student_class.studentid = student.id")
        ->where (["student_class.classid"=>$classname])
        ->all();
        

        return $this->asJson($students);

    }
}