<?php
use app\components\Breadcrumb;
use yii\widgets\ListView;

$this->title = "Page Analyzer"
?>
<?=Breadcrumb::widget(['title' => $this->title, 'icon' => 'fa fa-bar-chart'])?>
<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="tile">
            <div class="tile-title">Page List</div>
            <div class="tile-body">
                
            </div>
        </div>

    </div>
    <div class="col-sm-12 col-md-8">
    <?=

ListView::widget([
    'dataProvider' => $pages,
    'itemView' => '_list',
    'layout' => "{items}\n{pager}",
]);
?>
    </div>
</div>
