<?php

namespace app\modules\analyzer\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\data\ArrayDataProvider;

class PageAnalyzerController extends Controller
{
    public function actionIndex()
    {
        $path = FileHelper::normalizePath(Yii::getAlias('@app/controllers'), "\\");
        $path ="../controllers/";
        $result=[];
        $files=FileHelper::findFiles($path, ["fileTypes"=>"php"]);
        sort($files);
        foreach ($files as $file){
            $classname = "app\\controllers\\" .  basename($file, '.php');
            $reflectionClass = new \ReflectionClass($classname);
            $item = new \stdClass;
            $item->Controller = basename($file, 'Controller.php');
            $item->Actions = [];
            $publicMethods = $reflectionClass->getMethods(  \ReflectionMethod::IS_PUBLIC );

            $actionMethods = array_filter($publicMethods, function ($m) use ($classname){
                return $m->class == $classname && 
                    preg_match("/(action)[A-Z]/", $m->name);
             });

             foreach ($actionMethods as $action){
                array_push($item->Actions, str_replace("action", "", $action->name));
            }

             $codes ="\n";
             $fn = $reflectionClass->getMethod("behaviors");
             $codes .=sprintf("%s %d-%d\n", $fn->getFileName(), $fn->getStartLine(), $fn->getEndLine());
             $c = file($fn->getFileName());
             for ($i=$fn->getStartLine(); $i<=$fn->getEndLine(); $i++) {
               $codes .=sprintf('%04d %s', $i, $c[$i-1]);
             }
            $item->Behaviors =  $codes;
           $result[]=$item;
           $provider = new ArrayDataProvider(['allModels' => $result]);

        }
        
        return $this->render("index", ["pages"=>$provider]);
    }
    public function actionIndexx()
    {
        ob_clean();
        $path = FileHelper::normalizePath(Yii::getAlias('@app/controllers'), "\\");
        
        $classname = "app\controllers\SamplepageController";

       $rclass = new \ReflectionClass($classname);
       $methods = $rclass->getMethods(  \ReflectionMethod::IS_PUBLIC );

     
        $filtered = array_filter($methods, function ($m) use ($classname){
           return $m->class == $classname && 
            StringHelper::startsWith($m->name, "action");
        });

        foreach ($filtered as $m){
            echo str_replace( "action", "=", $m->name);
        }

        $rInstance = $rclass->newInstance (NULL,NULL);
        $mth = $rclass->getMethod("behaviors");
        $b = $mth->invoke($rInstance);
        var_dump ($b);



       exit;

    }
}