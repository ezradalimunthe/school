var tblTeacher = null;
$(function () {
    tblTeacher = $("#teacher").DataTable(_tableoptions);
  getTeacherTable()
})


render_firstname = function (data, type, row, meta) {
    if (type === 'display') {
        var btn = $("<a/>", {
 
            href: "/teacher/index/?id=" + row.id,
            text: row.first_name ,
            title:"Click untuk melihat detail data guru",
            data:{
                "toggle":"tooltip"
            }
        })
        return $(btn).prop('outerHTML');

    }
    return data;
}
render_lastname = function (data, type, row, meta) {
    if (type === 'display') {
        return row.last_name ;
    }
    return data;
}
render_detail = function (data, type, row, meta) {
    if (type === 'display') {


    }
    return data;
}
_tableoptions = {
    "paging": false,
    "ordering": true,
    "info": false,
    "searching": false,
    columns: [
        { name: "id", data: "id", "className": "text-right" },
        { name: "first_name", data: "first_name", render: render_firstname },
        { name: "last_name", data: "last_name", render: render_lastname },
        { name: "email", data: "email", "className": "text-center" },
        { name: "gender", data: "gender" },

    ]
}



getTeacherTable = function () {
    $.ajax({
        url: "/teacher/get-teacher-list",
        
        method: "POST",
        dataType: "json",
        success: function (r) {
            tblTeacher.rows.add(r).draw();
        }

    })
}