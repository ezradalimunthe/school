init = function () {
    /* console.log(ol);
    return; */
    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            center:[0,0], //ol.proj.fromLonLat([-74.006, 40.712]), // Coordinates of New York
            zoom: 1 //Initial Zoom Level
        })
    });

    var source = new ol.source.Vector({
        wrapX: false
    });
    var vector = new ol.layer.Vector({
        source: source
    });
    
    map.addLayer(vector);
   /*  console.clear();
    console.log(ol);
    return; */
    function addRandomFeature() {
         var x = 0//Math.random() * 360 - 180;
        var y = 0//Math.random() * 180 - 90;
        var geom = new ol.geom.Point(ol.proj.fromLonLat([x, y]));
        var feature = new ol.Feature(geom);
        source.addFeature(feature); 
    }

    var duration = 2000;
    function flash(feature) {
        var start = new Date().getTime();
        var listenerKey = map.on('postcompose', animate);

        function animate(event) {
            var vectorContext = event.vectorContext;
            var frameState = event.frameState;
            var flashGeom = feature.getGeometry().clone();
            var elapsed = frameState.time - start;
            var elapsedRatio = elapsed / duration;
            // radius will be 5 at start and 30 at end.
            var radius = ol.easing.easeOut(elapsedRatio) * 25 + 5;
            var opacity = ol.easing.easeOut(1 - elapsedRatio);

            var style = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: radius,
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, ' + opacity + ')',
                        width: 0.25 + opacity
                    })
                })
            });

            vectorContext.setStyle(style);
            vectorContext.drawGeometry(flashGeom);
          
            if (elapsed > duration) {
                ol.Observable.unByKey(listenerKey);
                return;
            }
            // tell OpenLayers to continue postcompose animation
            map.render();
        }
    }

    source.on('addfeature', function (e) {
        //flash(e.feature);
        console.log(e.feature)
        window.setInterval(function(){
            
            flash(e.feature);
        }, 2000);
    }); 

   // window.setInterval(addRandomFeature, 1000);
   addRandomFeature();
}

$(function () { init() })