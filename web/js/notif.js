//wrapper for http://bootstrap-notify.remabledesigns.com/

notif ={
	showError:function (title, message){
		this.showNotification(title, message, "danger", "fa fa-exlamation-triangle",0);
	},
	showWarning:function (title, message){
		this.showNotification(title, message, "warning", "fa fa-exclamation-circle",2000);
	},
	showInfo:function(title,message){
		this.showNotification(title, message, "info", "fa fa-info-circle",1000);
	},
	showNotification :function (title, message, type, icon, delay){
		$.notify({
			title: title,
			icon: icon,
			message: message
		}, {
				placement: {
					align: "right"
				},
				type: type,
				allow_dismiss:true,
				delay:delay,
				showProgressbar:delay!=0,
				template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} p-0" role="alert">' +
					'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
					'<h4 class="p-2"><span data-notify="icon"></span>&nbsp;&nbsp; ' +
					'<span  data-notify="title">{1}</span></h4>' +
					'<div class="bg-white p-2">'+
					'<span data-notify="message">{2}</span>' +
					'</div>'+
					'<div class="progress" data-notify="progressbar">' +
						'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
					'</div>' +
					'<a href="{3}" target="{4}" data-notify="url"></a>' +
				'</div>' 
			});
	}
}