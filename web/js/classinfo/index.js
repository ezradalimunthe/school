var tblStudent = null;
$(function () {
    tblStudent = $("#studenttable").DataTable(_tableoptions);
    $("#btnViewClass").click(_btnViewClass_click)

})

render_address = function (data, type, row, meta) {
    if (type === 'display') {
        return row.address + "  " + row.city;
    }
    return data;
}
render_fullname = function (data, type, row, meta) {
    if (type === 'display') {
        var btn = $("<a/>", {
 
            href: "/student/index/?id=" + row.id,
            text: row.firstname + "  " + row.lastname,
            title:"Click untuk melihat detail data siswa",
            data:{
                "toggle":"tooltip"
            }
        })
        return $(btn).prop('outerHTML');

    }
    return data;
}
render_detail = function (data, type, row, meta) {
    if (type === 'display') {


    }
    return data;
}
_tableoptions = {
    "paging": false,
    "ordering": true,
    "info": false,
    "searching": false,
    columns: [
        { name: "id", data: "id", "className": "text-right" },
        { name: "name", data: "firstname", render: render_fullname },
        { name: "city", data: "city", render: render_address },
        { name: "gender", data: "gender", "className": "text-center" },
        { name: "phone", data: "phone" },

    ]
}

_btnViewClass_click = function (e) {
    var selectedclass = $("#classname").val();
    var form = $(this).closest("form");
    var formaction = $(form).attr("action");
    tblStudent.clear();
    getStudentTable(selectedclass, formaction);
}

getStudentTable = function (studentclassname, url) {
    $.ajax({
        url: url,
        data: { "classname": studentclassname },
        method: "POST",
        dataType: "json",
        success: function (r) {
            tblStudent.rows.add(r).draw();
        }

    })
}