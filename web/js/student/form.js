$(function () {

    $("#form-data-siswa").on("beforeValidate", function (event, jqXHR, textStatus) {
       // $("#btnSubmit").attr('disabled', true).addClass('disabled');
    });
    $("#form-data-siswa").on("afterValidate", function (event, attribute, messages, deferreds) {

        var $errList = $("<ul>");
        
        if (attribute.error){
            notif.showError("Update Data Error!", attribute.error);
                return false;
        };
        if (messages.length > 0) {
            $.each(attribute, function (i, item) {
                if (item.length > 0) {
                    var $item = $("<li>", {
                        text: item.join(",")
                    })
                    $errList.append($item)
                }
            })
            var html = $($errList)[0].outerHTML;
            html = "<p>Perbaiki data ini:</p>" + html;
            notif.showWarning("Update data error", html);
            $("#btnSubmit").removeAttr('disabled', true).removeClass('disabled');
        }
    });

    $('#form-data-siswa').on('beforeSubmit', function (e) {
        notif.showInfo("Data tersimpan","Kembali ke halaman detail secara otomatis.")
        setTimeout(function () {
             window.location = "/student/index/?id=" + $("#student-id").val();
        }, 1500)
        return false
    });



})