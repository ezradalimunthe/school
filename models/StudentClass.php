<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_class".
 *
 * @property int $id
 * @property int $studentid fk from table student id
 * @property string $classid class id
 */
class StudentClass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_class';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['studentid'], 'integer'],
            [['classid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'studentid' => 'Studentid',
            'classid' => 'Classid',
        ];
    }
}
