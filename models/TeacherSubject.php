<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "teacher_subject".
 *
 * @property int $id
 * @property string $teacherid fk from table teacher id
 * @property int $subjectid fk from table subject id
 */
class TeacherSubject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teacher_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subjectid'], 'integer'],
            [['teacherid'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacherid' => 'Teacherid',
            'subjectid' => 'Subjectid',
        ];
    }
}
