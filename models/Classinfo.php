<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "classinfo".
 *
 * @property string $id
 * @property string $aliasshort short name for this class
 * @property string $aliaslong long name for this class
 */
class Classinfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classinfo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'aliasshort'], 'required'],
            [['id'], 'string', 'max' => 10],
            [['aliasshort', 'aliaslong'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'aliasshort' => 'Aliasshort',
            'aliaslong' => 'Aliaslong',
        ];
    }
}
