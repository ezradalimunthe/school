<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property int $id
 * @property string $firstname student firstname
 * @property string $lastname student lastname
 * @property string $gender student firstname
 * @property string $address student home address
 * @property string $city student home city
 * @property string $Phone parent phone
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required',"message"=>"Nomor induk siswa harus diisi."],
            [['id'], 'unique',"message"=>"Nomor induk siswa sudah ada."],
            [['id'], 'string', 'length'=>4,"message"=>"Nomor induk siswa harus 8 digit."],
            [['firstname', 'gender'], 'required'],
            [['city'],'required',"message"=>"kota harus diisi."],
            [['firstname', 'lastname', 'gender', 'address', 'city'], 'string', 'max' => 255],
            [['Phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'NIS',
            'firstname' => 'Nama Depan',
            'lastname' => 'Nama Belakang',
            'gender' => 'Gender',
            'address' => 'Alamat',
            'city' => 'Kota',
            'Phone' => 'Phone',
        ];
    }

    public function getClass()
    {
        $class= $this->hasOne(Classinfo ::className(), ['id' => 'classid'])
        ->viaTable(StudentClass::tableName(), ["studentid"=>'id']);
        return $class;
    }
}
